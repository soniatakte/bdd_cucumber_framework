package stepDefinitions;

import java.io.IOException;

import org.junit.Assert;

import CommonMethodpackage.Trigger_Patch_API_Method;
import RequestRepository.Endpoints;
import RequestRepository.Patch_RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Patch_API_StepDefinition {
	
	String req_body;
	String Patch_Endpoint;
	int res_Statuscode;
	String res_body;
	
	@Given("Enter Name and Job in requestbody")
	public void enter_name_and_job_in_requestbody() throws IOException {
		req_body = Patch_RequestRepository.Patch_TC1_Request();
		Patch_Endpoint = Endpoints.patch_endpoint();
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the Patch request with requestbody")
	public void send_the_patch_request_with_requestbody() {
		res_Statuscode = Trigger_Patch_API_Method.extract_Status_Code(req_body, Patch_Endpoint);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Success response is received {int} as status code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_Statuscode, 200);
		res_body = Trigger_Patch_API_Method.extract_Response_Body(req_body, Patch_Endpoint);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("responsebody as per API guide document")
	public void responsebody_as_per_api_guide_document() {
		JsonPath jsp_req = new JsonPath(req_body);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Create an object for ResponseBody
		JsonPath jsp_res = new JsonPath(res_body);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	    //throw new io.cucumber.java.PendingException();
	}




}
