package stepDefinitions;

import java.io.IOException;

import org.junit.Assert;

import CommonMethodpackage.Trigger_Put_API_Method;
import RequestRepository.Endpoints;
import RequestRepository.Put_Request_Repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Put_API_StepDefinition {

	String req_body;
	String Put_Endpoint;
	int res_Statuscode;
	String res_body;

	@Given("Enter Name and Job in RequestBody")
	public void enter_name_and_job_in_request_body() throws IOException {
		req_body = Put_Request_Repository.Put_TC1_Request();
		Put_Endpoint = Endpoints.put_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Put Request with RequestBody")
	public void send_the_put_request_with_request_body() {
		res_Statuscode = Trigger_Put_API_Method.extract_Status_Code(req_body, Put_Endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Success Response is received {int} as Status Code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_Statuscode, 200);
		res_body = Trigger_Put_API_Method.extract_Response_Body(req_body, Put_Endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("ResponseBody as per API guide document")
	public void response_body_as_per_api_guide_document() {
		JsonPath jsp_req = new JsonPath(req_body);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Create an object for ResponseBody
		JsonPath jsp_res = new JsonPath(res_body);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		// throw new io.cucumber.java.PendingException();
	}
	
}
