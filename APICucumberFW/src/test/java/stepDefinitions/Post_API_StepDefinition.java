package stepDefinitions;

import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import CommonMethodpackage.Trigger_API_Method;
import RequestRepository.Endpoints;
import RequestRepository.Post_RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Post_API_StepDefinition {

	String req_body;
	String Post_Endpoint;
	int res_Statuscode;
	String res_body;

	@Given("Enter Name and Job in Requestbody")
	public void enter_name_and_job_in_requestbody() throws IOException {
		req_body = Post_RequestRepository.Post_TC1_Request();
		Post_Endpoint = Endpoints.post_endpoint();
		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Post Request with Requestbody")
	public void send_the_post_request_with_requestbody() {
		res_Statuscode = Trigger_API_Method.extract_Status_Code(req_body, Post_Endpoint);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Success Response is received {int} as Status code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_Statuscode, 201);
		res_body=Trigger_API_Method.extract_Response_Body(req_body, Post_Endpoint);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Responsebody as per API guide document")
	public void responsebody_as_per_api_guide_document() {
		JsonPath jsp_req = new JsonPath(req_body);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);

		// Create an object for ResponseBody
		JsonPath jsp_res = new JsonPath(res_body);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt").substring(0, 11);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_createdAt);
		//throw new io.cucumber.java.PendingException();
	}

}
