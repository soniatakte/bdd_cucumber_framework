package stepDefinitions;

import org.junit.Assert;

import CommonMethodpackage.Trigger_Get_API_Method;

import RequestRepository.Endpoints;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Get_API_StepDefinition {

	String Get_Endpoint;
	int res_Statuscode;
	String res_body;

	@When("Send the Get request to the Endpoint")
	public void send_the_get_request_to_the_endpoint() {
		Get_Endpoint = Endpoints.get_endpoint();
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as Status code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_Statuscode, 200);
		res_body = Trigger_Get_API_Method.extract_Response_Body(Get_Endpoint);
		res_Statuscode = Trigger_Get_API_Method.extract_Status_Code(Get_Endpoint);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("response body as per API guide document")
	public void response_body_as_per_api_guide_document() {
		// Write code here that turns the phrase above into concrete actions
		//throw new io.cucumber.java.PendingException();
	}

}
