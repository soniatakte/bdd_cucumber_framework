package CommonUtilityPackage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_Logs {
	public static File Create_Log_Directory(String Directory_Name) {
		String Current_Project_Directory = System.getProperty("user.dir");
		System.out.println(Current_Project_Directory);
		File Log_Directory = new File(Current_Project_Directory + "//API_Logs//" + Directory_Name);
		Delete_Directory(Log_Directory);
		Log_Directory.mkdir();
		return Log_Directory;
		

	}

	public static boolean Delete_Directory(File Directory) {
		boolean Directory_deleted = Directory.delete();
		{
			if (Directory.exists()) {
				File[] files = Directory.listFiles();
				if (files != null) {
					for (File file : files) {
						if (file.isDirectory()) {
							Delete_Directory(file);
						} else {
							file.delete();
						}
					}
				}
				Directory_deleted = Directory.delete();
			} else {
				System.out.println("Directory does not exist.");
			}
			return Directory_deleted;

		}
	}

	public static void Evidence_Creator(File dirname, String filename, String endpoint, String requestbody,
			String responsebody) throws IOException {
		// Step1:Create a file at given location
		File newfile = new File(dirname + "\\" + filename + ".txt");
		System.out.println("newfile created to save evidence:" + newfile.getName());

		// Step2: Write data into the file
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("Endpoint is : " + endpoint + "\n\n");
		datawriter.write("RequestBody: \n\n" + requestbody + "\n\n");
		datawriter.write("ResponseBody: \n\n" + responsebody);
		datawriter.close();
		System.out.println("Evidence is written in file: " + newfile.getName());
	}
}
