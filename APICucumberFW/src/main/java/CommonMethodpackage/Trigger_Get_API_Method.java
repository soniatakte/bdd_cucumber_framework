package CommonMethodpackage;

import static io.restassured.RestAssured.given;

import RequestRepository.Endpoints;

public class Trigger_Get_API_Method extends Endpoints {
	public static int extract_Status_Code( String URL) {
		int StatusCode = given()
				.when().get(URL)
				.then().extract().statusCode();
		return StatusCode;
		}
		
		//Extract Responsebody
		
		public static String extract_Response_Body( String URL) {
			String ResponseBody = given()
					.when().get(URL)
					.then().extract().response().asString();
			return ResponseBody;
		}

}
