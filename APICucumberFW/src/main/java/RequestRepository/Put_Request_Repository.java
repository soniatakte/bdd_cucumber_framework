package RequestRepository;

import java.io.IOException;
import java.util.ArrayList;

import CommonUtilityPackage.Excel_Data_Reader;

public class Put_Request_Repository extends Endpoints {
	public static String Put_TC1_Request() throws IOException {

		ArrayList<String> Excel_Data = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "Put_API", "Put_TC_1");
		
		 //System.out.println(Excel_Data);

		String Req_name = Excel_Data.get(1);
		String Req_job = Excel_Data.get(2);

		String requestbody = "{\r\n" + "    \"name\": \"" + Req_name + "\",\r\n" + "    \"job\": \"" + Req_job
				+ "\"\r\n" + "}";
		return requestbody;
	}

}
